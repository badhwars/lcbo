package sheridan;

import sheridan.LiquourType;

import java.util.ArrayList;
import java.util.List;

public class LiquourService {
	

    public List<String> getAvailableBrands(LiquourType type){

        List<String> brands = new ArrayList( );

        if(type.equals(LiquourType.WINE)){
            brands.add("Adrianna Vineyard");
            brands.add(("J. P. Chenet"));
            
        }else if(type.equals(LiquourType.WHISKY)){
            brands.add("Glenfiddich");
            brands.add("Johnnie Wvfvfalker");
            brands.add("Jack Daniels");
            brands.add("Seagrams");
            

        }else if(type.equals(LiquourType.BEER)){
            brands.add("Corona");
            brands.add("Heineken");
            brands.add("Coors");
            brands.add("Canadian");
            brands.add("Budweiser");

        }else {
            brands.add("No Brand Available");
        }
    return brands;
    }
}
